# Universal Web-Based Data Browser and Editor for JetConf

This project aims at developing a universal data browser and editor for the [JetConf](https://gitlab.labs.nic.cz/labs/jetconf) network management application. This client-side web application is universal in the sense that it can work with any [YANG](https://datatracker.ietf.org/doc/html/rfc7950) data model implemented by the JetConf server. The user interface is generated automatically from the data model and no local customizations are required.  

The web application is built using the [Angular 2](https://angular.io/) framework and written in [TypeScript](https://www.typescriptlang.org).

## Installation

The following steps install the development environment and run the application on a local web server with automatic recompilation and restart after every change in the source code.
```
git clone git@gitlab.labs.nic.cz:jetconf/jetscreen.git
cd jetscreen
npm install
npm start
```
Prerequisites are [Node.js](https://nodejs.org) and [npm](https://www.npmjs.com) package manager.

## Building for production

```
npm install
npm run build
```

Processed files are placed in `public/`, and can be used with any webserver (eg. `cd public; python -m http.server`).
