const fs = require("fs");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const autoprefixer = require("autoprefixer");
const postcssUrl = require("postcss-url");
const cssnano = require("cssnano");
const {
  NoEmitOnErrorsPlugin,
  NamedModulesPlugin,
  NormalModuleReplacementPlugin,
  DefinePlugin
} = require("webpack");
const { CommonsChunkPlugin } = require("webpack").optimize;
const ParallelUglifyPlugin = require("webpack-parallel-uglify-plugin");
const { AotPlugin } = require("@ngtools/webpack");
const nodeModules = path.join(process.cwd(), "node_modules");
const realNodeModules = fs.realpathSync(nodeModules);
const genDirNodeModules = path.join(
  process.cwd(),
  "src",
  "$$_gendir",
  "node_modules"
);
const entryPoints = ["inline", "polyfills", "vendor", "main"];
const minimizeCss = process.env.NODE_ENV === "production";

const postcssPlugins = function() {
  const minimizeOptions = {
    autoprefixer: true,
    safe: true,
    mergeLonghand: false,
    discardComments: true
  };
  return [autoprefixer()].concat(minimizeCss ? [cssnano(minimizeOptions)] : []);
};

const dummyPlugin = function() {
  return { apply: function() {} };
};

module.exports = {
  devtool: process.env.NODE_ENV === "production" ? "source-map" : "eval",
  resolve: {
    extensions: [".ts", ".js", ".json", ".scss", ".html"]
  },
  entry: {
    main: ["./src/main.ts"],
    polyfills: ["./src/polyfills.ts"]
  },
  output: {
    path: path.join(process.cwd(), "public"),
    publicPath: process.env.PUBLIC_PATH || "/",
    filename:
      process.env.NODE_ENV === "production"
        ? "[name].[chunkhash:10].bundle.js"
        : "[name].bundle.js",
    chunkFilename:
      process.env.NODE_ENV === "production"
        ? "[id].[chunkhash:10].chunk.js"
        : "[id].chunk.js"
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader",
        exclude: [/\/node_modules\//]
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.html$/,
        loader: "raw-loader"
      },
      {
        test: /\.scss$|\.sass$/,
        use: [
          "exports-loader?module.exports.toString()",
          {
            loader: "css-loader",
            options: {
              sourceMap: false,
              importLoaders: 1
            }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: postcssPlugins
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: false,
              precision: 8,
              includePaths: []
            }
          }
        ]
      },
      {
        test: /\.ts$/,
        loader: "@ngtools/webpack"
      }
    ],
    exprContextCritical: false
  },
  plugins: [
    new AotPlugin({
      tsConfigPath: "./tsconfig.json",
      entryModule: path.join(process.cwd(), "./src/app/app.module#AppModule") // needs an absolute path: https://github.com/angular/angular-cli/issues/4913
    }),
    new NoEmitOnErrorsPlugin(),
    new NormalModuleReplacementPlugin(
      /\.\/environment\.dev/,
      "./environment.prod"
    ),
    new FaviconsWebpackPlugin({
      logo: "./icon.svg",
      prefix: "icons-[hash:10]/",
      title: "jetscreen",
      icons: {
        android: true,
        appleIcon: true,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        opengraph: true,
        twitter: true,
        yandex: false,
        windows: false
      }
    }),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
      chunksSortMode: function sort(left, right) {
        let leftIndex = entryPoints.indexOf(left.names[0]);
        let rightindex = entryPoints.indexOf(right.names[0]);
        if (leftIndex > rightindex) {
          return 1;
        } else if (leftIndex < rightindex) {
          return -1;
        } else {
          return 0;
        }
      }
    }),
    new CommonsChunkPlugin({ name: ["inline"], minChunks: null }),
    new CommonsChunkPlugin({
      name: ["vendor"],
      minChunks: module => {
        return (
          module.resource &&
          (module.resource.startsWith(nodeModules) ||
            module.resource.startsWith(genDirNodeModules) ||
            module.resource.startsWith(realNodeModules))
        );
      },
      chunks: ["main"]
    }),
    new NamedModulesPlugin()
  ],
  devServer: {
    historyApiFallback: true,
    stats: "minimal",
    watchOptions: {
      ignored: /node_modules|public/
    }
  }
};
