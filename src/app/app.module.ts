import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { ContainerNodeComponent } from './container-node.component';
import { ListNodeComponent } from './list-node.component';
import { InternalMembersComponent } from './internal-members.component';
import { DescriptionComponent } from './description.component';
import { TerminalMembersComponent } from './terminal-members.component';
import { ScalarValueComponent } from './scalar-value.component';
import { RestconfService } from './restconf.service';
import { requestOptionsProvider } from './default-request-options.service';
import { KeyLookupComponent } from './key-lookup.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [
    AppComponent,
    ContainerNodeComponent,
    ListNodeComponent,
    InternalMembersComponent,
    DescriptionComponent,
    TerminalMembersComponent,
    ScalarValueComponent,
    KeyLookupComponent
  ],
  providers: [
    RestconfService,
    requestOptionsProvider
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
