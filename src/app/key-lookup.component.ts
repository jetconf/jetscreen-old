import { Component, Input, OnInit } from '@angular/core';

import { TerminalObject } from './instance-data';
import { ListNode } from './tree-node';

@Component({
  selector: 'key-lookup',
  templateUrl: 'key-lookup.component.html',
  styleUrls: ['key-lookup.component.scss']
})
export class KeyLookupComponent implements OnInit {
  @Input()
  node: ListNode;
  keys: TerminalObject = {};
  failed: boolean = false;

  getKeys(): string[] {
    return Object.keys(this.keys);
  }

  onSubmit(): void {
    this.failed = !this.node.keyLookup(this.keys);
  }

  ngOnInit() {
    for (let k of this.node.digest.keys) {
      this.keys[k] = null;
    }
  }

}
