import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { TreeNode } from './tree-node';
import { SchemaDigest } from './schema-digest';
import { RestconfService } from './restconf.service';

@Component({
  selector: 'jetconf-client',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  root: TreeNode;

  constructor(public restconfService: RestconfService) {}

  setUrl(url: string): void {
    if (url.endsWith('/')) {
      url = url.slice(0, -1);
    }
    if (url !== this.restconfService.jetconfUrl) {
      this.restconfService.jetconfUrl = url;
      this.launch();
    }
  }

  private launch(): void {
    this.restconfService.getSchemaDigest()
      .subscribe(
        digest => this.createRoot(digest),
        error => console.log(error));
  }

  private createRoot(digest: SchemaDigest): void {
    this.root = new TreeNode('/', digest);
  }

  commit(): void {
    this.restconfService.commit()
      .subscribe(
        success => console.log('Commit successful.'),
        error => console.log(error));
  }

  reset(): void {
    this.root.toggleExpand();
    this.restconfService.reset()
      .subscribe(
        success => this.createRoot(this.root.digest as SchemaDigest),
        error => console.log(error));
  }

  selection(): [TreeNode, number] {
    let sel: TreeNode = this.root.selected ? this.root : this.root.selectedDescendant();
    if (sel && sel.populated) return [sel, sel.activeIndex];
    return null;
  }

  ngOnInit(): void {
    this.launch();
  }

}
