type EmptyValue = [null];
export type ScalarValue = boolean | number | string | EmptyValue;
export type TerminalValue = ScalarValue | ScalarValue[];
export type ObjectValue = {
  [name: string]: InstanceValue;
}
export type TerminalObject = {
  [name: string]: TerminalValue;
}
export type StructuredValue = ObjectValue | ObjectValue[];
export type InstanceValue = TerminalValue | StructuredValue;
