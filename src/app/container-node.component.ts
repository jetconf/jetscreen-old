import { Component, Input } from '@angular/core';

import { ObjectValue } from './instance-data';
import { TreeNode } from './tree-node';
import { RestconfService } from './restconf.service';

@Component({
  selector: 'container-node',
  templateUrl: 'container-node.component.html',
  styleUrls: ['./container-node.component.scss']
})
export class ContainerNodeComponent {
  @Input()
  private _node: TreeNode;

  constructor(private restconfService: RestconfService) {}

  @Input()
  set node(n: TreeNode) {
    this._node = n;
    if (!n.padre) this.handleExpand();
  }

  get node(): TreeNode { return this._node; }

  handleExpand(): void {
    this.node.toggleExpand();
    if (this.node.expanded && !this.node.populated) this.loadData();
  }

  handleSelect(): void {
    this.node.select();
    if (this.node.selected && !this.node.populated) this.loadData();
  }

  private loadData(): void {
    if (this.node.padre && this.node.padre.preloaded) {
      this.node.data = <ObjectValue>this.node.padre.memberData(this.node.name);
      this.node.preloaded = true;
      this.node.populate(this.node.data)
    } else {
      this.restconfService.getContent(this.node.resourceID(), 2)
        .subscribe(
          obj => this.node.populate(obj),
          error => this.handleError(error));
    }
  }

  handleError(error: string): void {
    this.node.expanded = false;
    this.node.selected = false;
    console.log(error);
  }

}
