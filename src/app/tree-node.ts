import { InstanceValue, TerminalObject, ObjectValue, StructuredValue } from './instance-data';
import { Digest, InternalDigest, ListDigest } from './schema-digest';

export class TreeNode {
  activeIndex: number = 0;
  expanded: boolean = false;
  selected: boolean = false;
  populated: boolean = false;
  preloaded: boolean = false;
  ninos: { [name: string]: TreeNode; };
  terminals: TerminalObject;
  padre: TreeNode = null;
  selectionHop: string = null;
  data: StructuredValue = null;

  constructor(public name: string, public digest: InternalDigest) { }

  get config(): boolean {
    return (this.digest.config === undefined ? this.padre.config : this.digest.config)
  }

  memberData(m: string): InstanceValue {
    return this.data[m];
  }

  toggleExpand(): void {
    this.expanded = !this.expanded;
    if (!this.expanded) {
      this.removeSelection();
      if (!this.selected) this.populated = false;
    }
  }

  cssClasses(): string[] {
    return [this.digest.kind, this.config ? 'config' : 'nonconfig'];
  }

  expansionTriangleClass(): string {
    let expanded = this.expanded ? 'expanded' : 'collapsed';
    let config = this.config ? 'config' : 'non-config';
    return [expanded, config].join(' ');
  }

  jsonPointer(): string {
    if (!this.padre) return '/';
    let res = '/' + this.pointerSegment();
    let node: TreeNode = this.padre;
    while (node.padre) {
      res = node.pointerSegment() + res;
      node = node.padre;
    }
    return res;
  }

  path(): string[] {
    let res: string[] = [];
    if (!this.padre) return res;
    let node: TreeNode = this.padre;
    while (node.padre) {
      res.unshift(node.resourceSegment());
      node = node.padre;
    }
    return res.concat(this.name);
  }

  resourceID(): string[] { return this.path(); }

  select(): void {
    if (this.selectionHop) { // descendant was selected
      this.deselectDescendant();
      this.selectionHop = null;
    } else {
      if (this.padre) {
        this.padre.propagateSelection(this.name);
      }
    }
    this.selected = true;
  }

  removeSelection(): void {
    if (this.selectionHop === null) return;
    let node: TreeNode = this;
    while (node !== null) {
      node.selectionHop = null;
      node = node.padre;
    }
    this.deselectDescendant();
  }

  selectedDescendant(): TreeNode {
    if (this.selectionHop === null) return null;
    let res: TreeNode = this;
    while (res.selectionHop) res = res.ninos[res.selectionHop];
    return (res.selected ? res : null);
  }

  private deselectDescendant(): void {
    if (this.selectionHop === null) return;
    let c: TreeNode = this.ninos[this.selectionHop];
    if (c.selected) {
      c.selected = false;
      if (!c.expanded) c.populated = false;
    } else {
      c.deselectDescendant();
      c.selectionHop = null;
    }
  }

  private propagateSelection(next: string): void {
    if (this.selectionHop) {
      this.deselectDescendant();
    } else {
      if (this.selected) {
        this.selected = false;
        if (!this.expanded) this.populated = false;
      } else {
        if (this.padre) this.padre.propagateSelection(this.name);
      }
    }
    this.selectionHop = next;
  }

  populate(obj: StructuredValue): void {
    this.ninos = {};
    this.terminals = {};
    for (let m in obj) {
      if (obj.hasOwnProperty(m)) {
        let cdig: Digest = this.digest.children[m];
        if (cdig === undefined) throw new Error(`member ${m} not in schema`);
        switch (cdig.kind) {
          case 'container':
            this.addInternal(new TreeNode(m, cdig));
            break;
          case 'list': // tslint:disable-next-line:no-use-before-declare
            this.addInternal(new ListNode(m, cdig));
            break;
          case 'leaf':
          case 'leaf-list':
            this.terminals[m] = obj[m];
            break;
        }
        this.populated = true;
      }
    }
  }

  protected pointerSegment(): string {
    return this.name
  }

  protected resourceSegment(): string {
    return this.name;
  }

  addInternal(member: TreeNode): void {
    member.padre = this;
    this.ninos[member.name] = member;
  }

  subtrees(): TreeNode[] {
    let res: TreeNode[] = [];
    for (let m of Object.keys(this.ninos).sort()) {
      res.push(this.ninos[m]);
    }
    return res;
  }

}

export class ListNode extends TreeNode {
  length: number = null;
  data: ObjectValue[] = null;

  constructor(public name: string, public digest: ListDigest) {
    super(name, digest);
  }

  memberData(m: string): InstanceValue {
    return this.data[this.activeIndex][m];
  }

  switchEntry(index: number): void {
    if (this.activeIndex === index) return;
    this.removeSelection();
    this.activeIndex = +index;
  }

  resourceID(): string[] {
    let res = this.path();
    res[res.length - 1] += this.keySpec();
    return res;
  }

  private keySpec(): string {
    return '=' + this.digest.keys.map(
      k => this.data[this.activeIndex][k]).join(',');
  }

  protected pointerSegment(): string {
    return `${this.name}/${this.activeIndex}`;
  }

  protected resourceSegment(): string {
    return this.name + this.keySpec();
  }

  keyLookup(ks: TerminalObject): boolean {
    entries:
    for (let i in this.data) {
      if (this.data.hasOwnProperty(i)) {
        let en: ObjectValue = this.data[i];
        for (let k of this.digest.keys) {
          if (en[k] !== ks[k]) continue entries;
        }
        this.switchEntry(+i);
        this.expanded = true;
        return true;
      }
    }
    return false;
  }

}
