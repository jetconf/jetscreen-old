import { Component, Input } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators, ValidatorFn } from '@angular/forms';

import { RestconfService } from './restconf.service';
import { TreeNode } from './tree-node';
import { ScalarValue, TerminalValue } from './instance-data';
import { InternalDigest, ListDigest, TerminalDigest } from './schema-digest';
import { YangType } from './schema-digest';

@Component({
  selector: 'terminal-members',
  templateUrl: 'terminal-members.component.html',
  styleUrls: ['terminal-members.component.scss']
})
export class TerminalMembersComponent {
  node: TreeNode;
  config: boolean = true;
  nonconfig: boolean = true;
  newMember: string = null;
  form: FormGroup;
  errors: string[] = [];

  constructor(private restconfService: RestconfService) { }

  @Input()
  set entry(n: [TreeNode, number]) {
    this.node = n[0];
    this.makeForm();
  }

  get members(): string[] {
    let keys: string[] = [];
    let other: string[] = [];
    for (let m of Object.keys(this.form.value)) {
      (this.isKey(m) ? keys : other).push(m);
    }
    return keys.concat(other.sort());
  }

  memberDigest(m: string): TerminalDigest {
    return this.node.digest.children[m] as TerminalDigest;
  }

  isKey(m: string): boolean {
    let dig: InternalDigest = this.node.digest;
    return dig.kind === 'list' && (<ListDigest>dig).keys.indexOf(m) >= 0;
  }

  isConfig(m: string): boolean {
    return this.node.config && this.memberDigest(m).config !== false;
  }

  isEditable(m: string): boolean {
    return this.isConfig(m) && !this.isKey(m);
  }

  validators(m: string): ValidatorFn[] {
    if (!this.isEditable(m)) return;
    let res: ValidatorFn[] = [];
    let td: YangType = this.memberDigest(m).type;
    switch (td.base) {
      case 'decimal64':
      case 'int8':
      case 'int16':
      case 'int32':
      case 'int64':
      case 'uint8':
      case 'uint16':
      case 'uint32':
      case 'uint64':
        break;
      case 'string':
        if (td.patterns) {
          res.concat(td.patterns.map(p => Validators.pattern(p)));
        }
        if (td.length) {
          let minl = td.length[0][0];
          if (minl !== null) {
            if (minl > 0) res.push(Validators.required);
            if (minl > 1) res.push(Validators.minLength(minl));
          }
          let maxl = td.length[td.length.length - 1][1];
          if (maxl !== null) {
            res.push(Validators.maxLength(maxl));
          }
        }
        break;
      case 'binary':
        break;
    }
    return res;
  }

  type(m: string): string {
    let mdig = this.memberDigest(m);
    return mdig.type.derived ? mdig.type.derived : mdig.type.base;
  }

  cssClasses(m: string): string[] {
    let res: string[] = [];
    if (this.isKey(m)) res.push('key');
    if (this.errors[m] !== undefined || !this.form.controls[m].valid) {
      res.push('invalid');
    }
    return res;
  }

  private makeForm(): void {
    this.form = new FormGroup({});
    for (let m in this.node.terminals) {
      if (this.node.terminals.hasOwnProperty(m)) {
        this.createMember(m, this.node.terminals[m]);
      }
    }
  }

  creatable(): string[] {
    let exist: string[] = Object.keys(this.form.value);
    let res: string[] = [];
    for (let m in this.node.digest.children) {
      if (this.node.digest.children.hasOwnProperty(m)) {
        let mdig = this.memberDigest(m);
        if (mdig.config !== false && exist.indexOf(m) < 0 &&
          (mdig.kind === 'leaf' || mdig.kind === 'leaf-list')) res.push(m);
      }
    }
    return res;
  }

  addNewMember(): void {
    this.createMember(this.newMember);
    this.newMember = null;
  }

  private createMember(m: string, value?: TerminalValue): void {
    let mdig = this.memberDigest(m);
    value = value === undefined ? mdig.default : value;
    let ctrl: AbstractControl;
    if (mdig.kind === 'leaf') {
      ctrl = new FormControl(value === undefined ? null : value);
    } else {
      ctrl = new FormArray([]);
      if (value !== undefined) {
        for (let v of <ScalarValue[]>value) {
          (<FormArray>ctrl).push(new FormControl(v));
        }
      }
    }
    this.form.addControl(m, ctrl);
  }

  sendData(): void {
    this.errors = [];
    let fval = this.form.value;
    for (let m in fval) {
      if (this.form.controls[m].pristine) continue;
      this.restconfService.put(this.node.resourceID().concat(m), fval[m])
        .subscribe(success => console.log('PUT:', `${this.node.jsonPointer()}/${m}`),
        error => this.errors[m] = error['error-message']);
    }
  }

  discardChanges(): void {
    this.makeForm();
    this.errors = [];
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

}
