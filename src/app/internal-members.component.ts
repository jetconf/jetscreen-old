import { Component, Input } from '@angular/core';

import { TreeNode } from './tree-node';

@Component({
  selector: 'internal-members',
  templateUrl: 'internal-members.component.html'
})
export class InternalMembersComponent {
  @Input()
  subtrees: TreeNode[];
}
