import { Component, Input, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';

import { YangType } from './schema-digest';

const enum TypeCategory {
  Boolean = 1,
  Enumeration = 2,
  Identityref = 3,
  Number = 4,
  String = 5
}

@Component({
  selector: 'scalar-value',
  templateUrl: 'scalar-value.component.html'
})
export class ScalarValueComponent {
  @Input()
  editable: boolean;
  @Input()
  type: YangType;
  @Input()
  form: FormControl;

  constructor(private elRef: ElementRef) {}

  showHelpText() {
    const helpText = this.elRef.nativeElement.nextElementSibling;
    if (helpText !== null) {
      helpText.classList.add('visible');
    }
  }

  hideHelpText() {
    const helpText = this.elRef.nativeElement.nextElementSibling;
    if (helpText !== null) {
      helpText.classList.remove('visible');
    }
  }

  formError(): string {
    let errs = this.form.errors;
    for (let e in errs) {
      if (errs.hasOwnProperty(e)) {
        switch (e) {
          case 'required':
            return 'must be non-empty';
          case 'minlength':
            return `minimum length is ${errs[e].requiredLength}`;
        }
      }
    }
  }

  category(): TypeCategory {
    switch (this.type.base) {
      case 'int8':
      case 'int16':
      case 'int32':
      case 'uint8':
      case 'uint16':
      case 'uint32':
        return TypeCategory.Number;
      case 'boolean':
        return TypeCategory.Boolean;
      case 'enumeration':
        return TypeCategory.Enumeration;
      case 'identityref':
        return TypeCategory.Identityref;
      default:
        return TypeCategory.String;
    }
  }

}
