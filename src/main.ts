import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppModule } from './app/app.module';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
} else {
  if (module['hot']) {
    let styles = Array.from(document.querySelectorAll('head style'));
    styles.forEach((style) => style.parentNode.removeChild(style));
    module['hot'].accept();
  }
}

platformBrowserDynamic().bootstrapModule(AppModule);
